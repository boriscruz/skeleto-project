package com.springboot.jsp.app.modelo.dao;

import com.springboot.jsp.app.modelo.dominio.Persona;

public interface IPersonaDAO extends IBaseDAO<Persona, Long> {

}
