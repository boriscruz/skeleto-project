package com.springboot.jsp.app.modelo.servicio;

import com.springboot.jsp.app.modelo.dominio.Persona;

public interface IPersonaServicio extends IBaseServicio<Persona, Long> {

}
