package com.springboot.jsp.app.modelo.servicio;

import java.util.List;

public interface IBaseServicio<T, ID> {

	List<T> listar();

	T obtenerPorId(ID id);
	
	T guardar(T entidad);
	
	T actualizar(T entidad);

	void eliminarPorId(ID id);
	
}
