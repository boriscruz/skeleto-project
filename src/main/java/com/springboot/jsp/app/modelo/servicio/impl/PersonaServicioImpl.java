package com.springboot.jsp.app.modelo.servicio.impl;

import org.springframework.stereotype.Service;

import com.springboot.jsp.app.modelo.dominio.Persona;
import com.springboot.jsp.app.modelo.servicio.IPersonaServicio;

@Service
public class PersonaServicioImpl extends BaseServicioImpl<Persona, Long> implements IPersonaServicio {

}
