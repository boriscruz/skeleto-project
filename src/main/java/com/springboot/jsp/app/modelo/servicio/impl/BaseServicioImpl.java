package com.springboot.jsp.app.modelo.servicio.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.jsp.app.modelo.dao.IBaseDAO;
import com.springboot.jsp.app.modelo.servicio.IBaseServicio;

@Service
public class BaseServicioImpl<T, ID> implements IBaseServicio<T, ID> {

	@Autowired
	private IBaseDAO<T, ID> baseDAO;

	@Override
	public List<T> listar() {
		return baseDAO.findAll();
	}

	@Override
	public T obtenerPorId(ID id) {
		return baseDAO.getOne(id);
	}

	@Override
	public T guardar(T entidad) {
		return baseDAO.save(entidad);
	}

	@Override
	public T actualizar(T entidad) {
		return baseDAO.save(entidad);
	}

	@Override
	public void eliminarPorId(ID id) {
		baseDAO.deleteById(id);
	}

}
