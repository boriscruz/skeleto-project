package com.springboot.jsp.app.modelo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IBaseDAO<T, ID>  extends JpaRepository<T, ID> {

}
