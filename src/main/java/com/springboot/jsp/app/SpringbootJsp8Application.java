package com.springboot.jsp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJsp8Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJsp8Application.class, args);
	}

}
