package com.springboot.jsp.app.controlador;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.springboot.jsp.app.modelo.dominio.Persona;
import com.springboot.jsp.app.modelo.servicio.IPersonaServicio;

@Controller
public class PrincipalControlador {

	@Autowired
	private IPersonaServicio personaServicio;

	@GetMapping(value = "/")
	public String index() {
		return "index";
	}

	@PostMapping(value = "/guardarPersona")
	public ResponseEntity<Persona> guardarPersona(HttpServletRequest request) {
		System.out.println("Guardar persona");
		
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		
		Persona persona = new Persona();
		
		persona.setNombre(nombre);
		persona.setApellido(apellido);
		
		try {
			return new ResponseEntity<Persona>(personaServicio.guardar(persona), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Persona>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
